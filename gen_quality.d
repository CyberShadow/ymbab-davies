import std.conv;
import std.format;
import std.stdio;

import ae.utils.xmlbuild;

enum margin = 50;

enum maxQuality = 70;
enum minQuality = 20;
enum qualitySize = 300;
enum qualityScale = qualitySize / (maxQuality - minQuality);
enum maxClicks = 60;
enum clicksSize = 400;
enum clickScale = double(clicksSize) / maxClicks;

void main()
{
	auto svg = newXml().svg();
	svg.xmlns = "http://www.w3.org/2000/svg";
	svg["version"] = "1.1";
	svg.width  = text(margin * 2 + clicksSize);
	svg.height = text(margin * 2 + qualitySize);

	// svg.rect(["width" : 1000.text, "height" : 1000.text, "fill" : "black"]);

	static struct Tier
	{
		string name, color;
		int base; double min, max;
	}
	static immutable Tier[] tiers = [
		Tier("thrilled"  , "#0000FF", 20, 0.7, 0.8),
		Tier("pleased"   , "#00FF00", 22, 0.6, 0.7),
		Tier("displeased", "#FF0000", 24, 0.4, 0.6),
		Tier("angered"   , "#888888", 26, 0.4, 0.6),
	];

	enum legendX = margin +  clicksSize * 0.1;
	enum legendY = margin + qualitySize * 0.1;
	enum legendW = 120;
	enum legendRowH = 20;
	enum legendMargin = 10;
	enum legendH = legendRowH * tiers.length + legendMargin * 2;
	svg.rect([
		"x" : text(legendX),
		"y" : text(legendY),
		"width" : text(legendW),
		"height" : text(legendH),
		"fill" : "black",
		"stroke" : "white",
		"stroke-width" : "3px",
	]);

	double svgX(double clicks) { return margin + clicks * clickScale; }
	double svgY(double quality) { return margin + qualitySize - (quality - minQuality) * qualityScale; }

	foreach_reverse (tierIndex, tier; tiers)
	{
		auto path = svg.path();
		path.d = "M %s,%s L %s,%s L %s,%s z".format(
			svgX(0), svgY(tier.base),
			svgX(maxClicks), svgY(tier.base + tier.min * maxClicks),
			svgX(maxClicks), svgY(tier.base + tier.max * maxClicks),
		);
		// path.stroke = tier.color;
		// path.fill = "none";
		path.fill = tier.color;
		path.style = "mix-blend-mode: screen";
	
		svg.rect([
			"x"      : text(legendX + legendMargin),
			"y"      : text(legendY + legendMargin + tierIndex * legendRowH),
			"width"  : text(16),
			"height" : text(16),
			"fill"   : tier.color,
		]);
		svg.text([
			"font-family" : "sans-serif",
			"font-weight" : "bold",
			"fill" : "white",
			"text-anchor" : "middle",
			"transform" : "translate(%s,%s)".format(
				legendX + legendMargin + 20,
				legendY + legendMargin + tierIndex * legendRowH + 13,
			),
			"fill" : "white",
			"text-anchor" : "start",
		])[] = tier.name;
	}

	{
		auto axes = svg.path();
		axes.fill = "none";
		axes.stroke = "white";
		axes["stroke-width"] = "4px";
		axes.d =
			"M %s,%s v -%s l -5,10 h 10 l -5,-10 l -5,10".format(margin, margin + qualitySize, qualitySize + 20)
			~ " " ~
			"M %s,%s h  %s l -10,-5 v 10 l 10,-5l -10,-5".format(margin, margin + qualitySize, clicksSize  + 20)
			;
	}

	auto texts = svg.g();
	texts["font-weight"] = "bold";
	texts["font-family"] = "sans-serif";
	texts["fill"] = "white";
	texts["text-anchor"] = "middle";

	texts.text([
		"font-size" : "30pt",
		"transform" : "translate(%s,%s) rotate(-90)".format(
			margin / 2 + 15,
			margin + qualitySize / 2,
		),
	])[] = "QUALITY";
	texts.text([
		"font-size" : "12pt",
		"transform" : "translate(%s,%s)".format(
			margin / 2,
			margin + 5,
		),
	])[] = "+%s".format(maxQuality);
	texts.text([
		"font-size" : "12pt",
		"transform" : "translate(%s,%s)".format(
			margin / 2,
			margin + qualitySize + 5,
		),
	])[] = "+%s".format(minQuality);

	texts.text([
		"font-size" : "20pt",
		"transform" : "translate(%s,%s)".format(
			margin + 0.5 * clicksSize,
			margin + qualitySize + margin / 2 + 10,
		),
	])[] = "TOTAL CLICKS";
	texts.text([
		"font-size" : "12pt",
		"transform" : "translate(%s,%s)".format(
			margin,
			margin + qualitySize + 30,
		),
	])[] = "0";
	texts.text([
		"font-size" : "12pt",
		"transform" : "translate(%s,%s)".format(
			margin + clicksSize,
			margin + qualitySize + 30,
		),
	])[] = "60";

	// texts.text([
	// 	"font-size" : "16pt",
	// 	"transform" : "translate(%s,%s)".format(
	// 		margin + clicksSize / 4,
	// 		margin + (qualitySize / 4),
	// 	),
	// 	"fill" : "#004400"
	// ])[] = "thrilled";
	// texts.text([
	// 	"font-size" : "16pt",
	// 	"transform" : "translate(%s,%s)".format(
	// 		margin + clicksSize * 11 / 16,
	// 		margin + (qualitySize / 2),
	// 	),
	// 	"fill" : "#222200"
	// ])[] = "pleased";

	// texts.text([
	// 	"font-size" : "16pt",
	// 	"transform" : "translate(%s,%s) rotate(-40)".format(
	// 		margin + clicksSize * 13 / 16,
	// 		margin + (qualitySize * 11 / 16),
	// 	),
	// 	"fill" : "#440000"
	// ])[] = "displeased";

	// texts.text([
	// 	"font-size" : "16pt",
	// 	"transform" : "translate(%s,%s)".format(
	// 		margin + clicksSize * 13 / 16,
	// 		margin + (qualitySize * 15 / 16),
	// 	),
	// 	"fill" : "#220022"
	// ])[] = "angered";

	svg.toString().toFile("quality.svg");
}
