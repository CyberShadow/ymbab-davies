import std.algorithm.iteration;
import std.conv;
import std.format;
import std.stdio;
import std.string;
import std.traits;

import common;

void main()
{
	foreach (lucky; [false, true])
	{
		auto f = File("21-%d.steamguide".format(lucky), "wb");
		f.writefln("Optimal Strategy (%s \"Lucky\")", lucky ? "with" : "without");
		f.writeln();
		
		void genTable(Res res, int riskPct, size_t tierIndex)
		{
			f.writefln("[h3]%d%% \"%s\"%s :: %s[/h3]",
				riskPct, tiers[tierIndex].name, tierIndex ? " or better" : "",
				res.text.capitalize,
			);
			f.writeln();
			f.writefln("[table][tr][th]Your %s[/th][th]Optimal Clicks[/th][/tr]",
				res.text.capitalize
			);
			f.write("[tr][td]0 to ");

			string clickDesc(int clicks)
			{
				return clicks == -1
					? "not possible"
					: "%d click%s (%d%%)".format(clicks, clicks == 1 ? "" : "s", clicks * 100 / 20);
			}

			auto risk = riskPct / 100.0;
			// auto tier = &tiers[tierIndex];

			enum max = 100_000;
			auto lastClicks = -2;
			foreach (amt; 0 .. max)
			{
				int bestClicks = -1;
				foreach (clicks; 0 .. 20 + 1)
				{
					auto score = getScore(res, amt / 20.0 * clicks, clicks);
					auto probs = calcProbs(score, lucky);

					auto cumulativeProbability = sum(probs[0 .. tierIndex + 1]);

					if (cumulativeProbability >= risk)
					{
						bestClicks = clicks;
						break;
					}
				}

				if (bestClicks != lastClicks)
				{
					if (amt)
					{
						f.writef("%d[/td][td]%s[/td][/tr]\n[tr][td]%d to ",
							amt - 1,
							clickDesc(lastClicks),
							amt,
						);
					}
					lastClicks = bestClicks;
				}
			}
			f.writefln("∞[/td][td]%s[/td][/tr]",
				clickDesc(lastClicks),
			);
			f.writeln("[/table]");
			f.writeln();
		}

		void genTables(int riskPct, size_t tierIndex)
		{
			foreach (res; EnumMembers!Res)
				genTable(res, riskPct, tierIndex);
		}

		genTables(100, 0);
		genTables( 99, 1);
		genTables( 90, 1);
		// genTables(res,  90, 2);
	}
}
