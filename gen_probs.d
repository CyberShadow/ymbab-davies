import std.algorithm.iteration;
import std.conv;
import std.format;
import std.stdio;

import ae.utils.xmlbuild;

import common;

enum margin = 50;

enum scoreScale = 1.5;
enum probScale = 300 * scoreScale;

void main()
{
	foreach (lucky; [false, true])
	{
		auto svg = newXml().svg();
		svg.xmlns = "http://www.w3.org/2000/svg";
		svg["version"] = "1.1";
		svg.width  = text(margin * 2 + maxScore * scoreScale);
		svg.height = text(margin * 2 + probScale);

		// svg.rect(["width" : 1000.text, "height" : 1000.text, "fill" : "black"]);

		foreach_reverse (tierIndex, tier; tiers)
		{
			string d = "M %s,%s".format(margin, margin + probScale);
			foreach (score; 0 .. maxScore + 1)
			{
				auto probs = calcProbs(score, lucky);

				double cumulativeProbability = probs[0 .. tierIndex + 1].sum;
				auto x = margin + score                       * scoreScale;
				auto y = margin + (1 - cumulativeProbability) *  probScale;
				d ~= " L %s,%s".format(x, y);
			}
			d ~= "L %s,%s z".format(margin + maxScore * scoreScale, margin + probScale);
			auto path = svg.path();
			path.d = d;
			path.fill = tier.color;
		}

		{
			auto axes = svg.path();
			axes.fill = "none";
			axes.stroke = "white";
			axes["stroke-width"] = "4px";
			axes.d =
				"M %s,%s v -%s l -5,10 h 10 l -5,-10 l -5,10".format(margin, margin + maxScore * scoreScale, maxScore * scoreScale + 20)
				~ " " ~
				"M %s,%s h  %s l -10,-5 v 10 l 10,-5l -10,-5".format(margin, margin + maxScore * scoreScale, probScale             + 20)
				;
		}

		auto texts = svg.g();
		texts["font-weight"] = "bold";
		texts["font-family"] = "sans-serif";
		texts["fill"] = "white";
		texts["text-anchor"] = "middle";

		texts.text([
			"font-size" : "20pt",
			"transform" : "translate(%s,%s) rotate(-90)".format(
				margin / 2 + 15,
				margin + (maxScore * scoreScale) / 2,
			),
		])[] = "PROBABILITY OF RESULT";
		texts.text([
			"font-size" : "12pt",
			"transform" : "translate(%s,%s)".format(
				margin / 2,
				margin + 5,
			),
		])[] = "100%";
		texts.text([
			"font-size" : "12pt",
			"transform" : "translate(%s,%s)".format(
				margin / 2,
				margin + (maxScore * scoreScale) + 5,
			),
		])[] = "0%";

		texts.text([
			"font-size" : "30pt",
			"transform" : "translate(%s,%s)".format(
				margin + 0.5 * probScale,
				margin + (maxScore * scoreScale) + margin / 2 + 10,
			),
		])[] = "SCORE";
		texts.text([
			"font-size" : "12pt",
			"transform" : "translate(%s,%s)".format(
				margin,
				margin + (maxScore * scoreScale) + 30,
			),
		])[] = "0";
		texts.text([
			"font-size" : "12pt",
			"transform" : "translate(%s,%s)".format(
				margin + probScale,
				margin + (maxScore * scoreScale) + 30,
			),
		])[] = "300";

		double[2][] textCoords;
		if (!lucky)
			textCoords = [
				[12 / 16., 12. / 16.],
				[ 7 / 16.,  6. / 16.],
				[ 5 / 16.,  4. / 16.],
				[ 3 / 16.,  1. / 16.],
			];
		else
			textCoords = [
				[12 / 16., 12. / 16.],
				[ 7 / 16.,  4. / 16.],
				[13 / 64., 16. / 64.],
				[ 2 / 16.,  1. / 16.],
			];

		texts.text([
			"font-size" : "16pt",
			"transform" : "translate(%s,%s)".format(
				margin + probScale               * textCoords[0][0],
				margin + (maxScore * scoreScale) * textCoords[0][1],
			),
			"fill" : "#004400"
		])[] = "thrilled";
		texts.text([
			"font-size" : "16pt",
			"transform" : "translate(%s,%s)".format(
				margin + probScale               * textCoords[1][0],
				margin + (maxScore * scoreScale) * textCoords[1][1],
			),
			"fill" : "#222200"
		])[] = "pleased";

		texts.text([
			"font-size" : "16pt",
			"transform" : "translate(%s,%s) rotate(%d)".format(
				margin + probScale               * textCoords[2][0],
				margin + (maxScore * scoreScale) * textCoords[2][1],
				lucky ? -65 : -50,
			),
			"fill" : "#440000"
		])[] = "displeased";

		texts.text([
			"font-size" : "16pt",
			"transform" : "translate(%s,%s)".format(
				margin + probScale               * textCoords[3][0],
				margin + (maxScore * scoreScale) * textCoords[3][1],
			),
			"fill" : "#220022"
		])[] = "angered";

		svg.toString().toFile("probs-%d.svg".format(lucky));
	}
}
