struct Tier
{
	string name, color;
	int exponent;
}
immutable Tier[] tiers = [
	Tier("thrilled"  , "#88FF88", 1),
	Tier("pleased"   , "#FFFF44", 3),
	Tier("displeased", "#FF8888", 6),
	Tier("angered"   , "#FF44FF", 0),
];

enum Res
{
	gold,
	power,
	thought,
}

enum maxScore = 300;

float getScore(Res res, double amount, int clicks)
{
	final switch (res)
	{
		case Res.gold   : return amount * 0.15 + clicks * 2.5;
		case Res.power  : return                 clicks * 2.5;
		case Res.thought: return amount * 0.1  + clicks * 2.5;
	}
}

double[tiers.length] calcProbs(double score, bool lucky)
{
	double[tiers.length] probs = 0;
	double remainingProb = 1.0;

	void doProb(size_t resultTier)
	{
		auto threshold = score;
		auto stopProb = threshold / 300.0;
		if (stopProb > 1)
			stopProb = 1;
		probs[resultTier] += stopProb * remainingProb;
		remainingProb *= 1 - stopProb;
	}
	doProb(0);
	doProb(1);
	doProb(1);
	if (lucky)
	{
		doProb(0);
		doProb(1);
		doProb(1);
	}
	doProb(2);
	doProb(2);
	doProb(2);
	probs[3] = remainingProb;

	return probs;
}
