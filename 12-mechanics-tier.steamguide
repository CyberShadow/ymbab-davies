Mechanics Step 2: Tier

Using the score calculated in the previous step, the game uses the following algorithm:

[olist]
[*]Pick a number between 0 and 300 (inclusive). If it is below the score, the resulting tier is "thrilled". Otherwise, proceed to step 2.
[*]Pick a number between 0 and 300 (inclusive). If it is below the score, the resulting tier is "pleased". Otherwise, proceed to step 3.
[*]Pick a number between 0 and 300 (inclusive). If it is below the score, the resulting tier is "pleased". Otherwise, proceed to step 4.
[*]Pick a number between 0 and 300 (inclusive). If it is below the score, the resulting tier is "displeased". Otherwise, proceed to step 5.
[*]Pick a number between 0 and 300 (inclusive). If it is below the score, the resulting tier is "displeased". Otherwise, proceed to step 6.
[*]Pick a number between 0 and 300 (inclusive). If it is below the score, the resulting tier is "displeased". Otherwise, the resulting tier is "angered".
[*]If the player has the "Lucky" trait, and the resulting tier is "displeased" or worse, retry the above steps one time. Otherwise, use the result as-is.
   The "Lucky" trait is obtained by recruiting all monsters in the "Team Mana" group, including the secret Wraith monster.
[/olist]

Note the following outcomes of the above:

1. The "thrilled" tier is guaranteed to be granted with a net score of over 300.
2. Even a meager offering has a small chance to produce the "thrilled" result.
2. For any net score value below 300, the full range of result tiers is possible (anything from "thrilled" to "angered"), at different probabilities.

Mathematically, the chance for each tier corresponds to the score divided by 300 and to the power of 1, 3, and 6 respectively.

As a visualization, you can consult the following graph:

[h3]Probability chart - without "Lucky"[/h3]
[previewicon=probs-0.png;sizeOriginal][/previewicon]

[h3]Probability chart - with "Lucky"[/h3]
[previewicon=probs-1.png;sizeOriginal][/previewicon]
